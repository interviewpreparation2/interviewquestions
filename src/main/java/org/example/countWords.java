package org.example;

import java.util.StringTokenizer;

public class countWords {
    public static void main(String[] args) {

        String str = "java is the programming language";
        int count = countWordsUsingStringTokenizer(str);
        System.out.println("Total no of words are : "+count);
    }
    public static int countWordsUsingStringTokenizer(String sentence) {
        if (sentence == null || sentence.isEmpty()) {
            return 0;
        }
        StringTokenizer tokens = new StringTokenizer(sentence);
        return tokens.countTokens(); }


}
