package org.example;

import java.util.StringTokenizer;

public class splitString {

    public static void main(String[] args) {

        String sentence = "java is the programming language";
        //using in build method
        String [] splict = sentence.split(" ");
        for (String spl:splict) {
            System.out.println("splinted  words : "+spl);
        }

        //using in StringTokenizer method
        StringTokenizer tokens = new StringTokenizer(sentence);
        while (tokens.hasMoreTokens()){
            String str = tokens.nextToken();
            System.out.println("splinted  words : "+str);
        }
    }

}
