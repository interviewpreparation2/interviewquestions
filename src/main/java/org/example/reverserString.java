package org.example;

public class reverserString {

    public static void main(String[] args) {

        String str = "java is the programming language";
        String [] str1 = str.split(" ");
        String reversedString = "";
        for (int i=0;i<str1.length;i++){
            String word = str1[i];
            String reverseWord = "";
            for (int j = word.length() - 1; j >= 0; j--) {
                reverseWord = reverseWord + word.charAt(j);
            }
            reversedString = reversedString + reverseWord + " ";
        }
        // Displaying the string after reverse
        System.out.println("Reversed string in same order: " + reversedString);
        String rev = reverse(str);
        System.out.println("Reversed string in rev order: " + rev);
    }

    public static String reverse(String input) {

        if (input == null) return null;

        StringBuilder output = new StringBuilder();

        for (int i = input.length() - 1; i >= 0; i--) {
            output.append(input.charAt(i));
        }

        return output.toString();
    }

}
